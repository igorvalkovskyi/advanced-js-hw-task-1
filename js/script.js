//Якщо об'єкт B є прототипом об'єкта A, то кожен раз, коли у B є властивість, наприклад колір, A успадкує той же самий колір, якщо інше не вказано.
//Для того,щоб викликати батьківський метод


class Employee{
constructor(name,age,salary){
    this.name = name;
    this.age = age;
    // this.salary = salary;
}

get name(){
    return this._name;
}
get age(){
    return this._age;
}
get salary(){
    return this._salary;
}
set name(newName) {
    this._name = newName;
  }

  set age(newAge) {
    this._age = newAge;
  }

  set salary(newSalary) {
    this._salary = newSalary;
  }
}



class Programmer extends Employee{
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
      }
      get salary() {
        return this._salary * 3;
      }
}
const programmer1 = new Programmer("Сергій", 21, 35000, ["JavaScript", "HTML/CSS"]);
const programmer2 = new Programmer("Ігор", 27, 53500, ["Python", "Java"]);
const programmer3 = new Programmer("Андрій", 24, 22360, ["C++", "C#"]);

console.log(programmer1);
console.log(programmer2);
console.log(programmer3);

